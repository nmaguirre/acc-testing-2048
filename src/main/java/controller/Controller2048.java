package controller;

import model.Board2048;

/**
 * This class takes care of the control of the game.
 *
 */
public class Controller2048 {

	private Board2048 board;
	
	public Controller2048(Board2048 board) {
		if (board==null) throw new IllegalArgumentException("board no debe ser nulo");
		this.board = board;
	}

	public Controller2048() {
		board = new Board2048();
	}

	public boolean isFinished() { 
		return false;
	}

	public void down() { }
	
	public void up() { }
	
	public void left() { }
	
	public void right() { }

	public void setTablero(Board2048 tablero) {
		if (tablero==null) throw new IllegalArgumentException("tablero no deberia ser nulo");
		this.board = tablero;		
	}

	public Board2048 getTablero() {
		return this.board;
	}
	
}
