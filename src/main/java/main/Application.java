package main;

import main.Pair;
import controller.Controller2048;
import model.Board2048;

/** 
 * Main class for 2048 app
 *  
 */
public class Application {
	
	private Controller2048 controller;
	private Board2048 board;

	public Application() {
		controller = new Controller2048();
		board = controller.getTablero();
	}

	/**
	 * Starts the game
	 */
	public void juegoNuevo() {
		board = new Board2048();
		controller.setTablero(board);
	}

	public Board2048 getBoard() {
		return board;
	}

	public void moveDown() {
		this.board.moveDown();
	}

	public Pair getLastRandomCoordinates() {
		return this.board.getLastRandomCellCoordinates();
	}
	
	
}
