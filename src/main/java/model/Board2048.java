package model;
import java.util.Random;

import main.Pair;

/**
 * Class that represents the state of the 2048 game. It takes care of the logic of the game too.
 *
 */

public class Board2048 {

	/**
	 * Stores the game board. Indices for cells must go from 1 to 4, both
	 * for rows and for columns.
	 */
	private Cell2048[][] elements;
	
	private static final int ROWS = 4;
	private static final int COLUMNS = 4;
	
	
	/**
	 * Tuple containing the coordinates of the last random cell.
	 */
	private Pair lastRandomCellCoordinates;

	public Pair getLastRandomCellCoordinates() {	
		return lastRandomCellCoordinates;
	}

	/**
	 * Creates a board of 4x4 cells, with all cells with default value (unset)
 	 * except for two randomly picked cells, which must be set with eithers 2 or 4.
	 * Values to set the filled cells are chosen randomly. Positions of the two filled
	 * cells are chosen randomly.
	 */
	public Board2048() {
		elements = new Cell2048[ROWS][COLUMNS]; 
		for(int i=0; i < ROWS; i++){
			for(int j=0; j<COLUMNS; j++){
				elements[i][j] = new Cell2048();
			}
		}
		Random generador = new java.util.Random();
		int r1 = Math.abs((generador.nextInt()%ROWS));
		int r2 = Math.abs((generador.nextInt()%COLUMNS));
		elements [r1][r2].setValue((Math.abs(generador.nextInt())%2+1)*2);

	    int r3 =  Math.abs((generador.nextInt()%ROWS));
		int r4 =  Math.abs((generador.nextInt()%COLUMNS));
		while (r1==r3 && r2==r4){
		    r3 =  Math.abs((generador.nextInt()%ROWS));
			r4 =  Math.abs((generador.nextInt()%COLUMNS));
		}
		elements [r3][r4].setValue((Math.abs(generador.nextInt())%2+1)*2);
		
		
		
	}
	
	/**
	 * Completely clears the board contents.
	 */
	public void clearBoard() {
		//TODO Implement this method
	}
	
	/**
	 * Indicates whether the game is finished or not.
	 * Game finishes when either 2048 is reached, or if there is no possible movement.
	 * @return true iff the game is finished.
	 */
	public boolean isFinished() {
		if (isWinningBoard()) return true;
		return (isFull() && !canMoveRight() && !canMoveLeft() &&
				!canMoveDown() && !canMoveUp());
	}
	
	/**
	 * Number of rows in the board. Should be constantly 4
	 * @return number of rows in the board.
	 */
	public int numberOfRows() {
		return this.ROWS;
	}

	/**
	 * Number of columns in the board. Should be constantly 4
	 * @return number of columns in the board.
	 */
	public int numberOfColumns() {
		return this.COLUMNS;
	}

	/**
	 * Returns the number of filled cells in the board
	 * @return number of filled cells in the board.
	 */
	public int numberOfFilledCells() {
		int cont=0;
		for(int row=0; row<ROWS;row++){
			for(int col=0; col<COLUMNS;col++){
				if(!this.elements[row][col].isAvailable())
					cont++;		
			}
		}
		return cont;
	}

	/**
	 * provides a string representation of the board content.
	 */
	public String toString() {
		String res = "---------";
		for (int row=0; row<this.numberOfRows(); row++) {
			res = res + "\n|";
			for (int col=0; col<this.numberOfColumns(); col++) {
				String value = " ";
				if (!this.elements[row][col].isAvailable()) {
					value = this.elements[row][col].getValue()+"";
				}
				res = res + value + "|";
			}
		}
		res = res + "\n---------";
		return res;
	}
	
	/**
	 * Indicates if all cells in the board are set or not
	 */
	public boolean isFull() {
		for(int i = 0; i < ROWS; i++){
			for(int j = 0; j < COLUMNS; j++){
				if(this.elements[i][j].isAvailable()){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Indicates whether the board would change through a movement to the left
	 */
	public boolean canMoveLeft() {
		//TODO Implement this method
		return false;
	}

	/**
	 * Indicates whether the board would change through a movement to the right
	 */
	public boolean canMoveRight() {
		//TODO Implement this method
		return false;
	}

	/**
	 * Indicates whether the board would change through a movement up
	 */
	public boolean canMoveUp() {
		//TODO Implement this method
		return false;
	}

	/**
	 * Indicates whether the board would change through a movement down
	 */
	public boolean canMoveDown() {
		for (int j = 0; j < COLUMNS ; j++) {
			for (int i = ROWS-1; i > 0 ; i--){
				if(this.elements[i][j].isAvailable()){
					if(!(this.elements[i-1][j]).isAvailable())
						return true;
				}else{
					if(!(this.elements[i-1][j]).isAvailable()){
						if(this.elements[i][j].getValue() == this.elements[i-1][j].getValue())
							return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Indicates whether 2048 is present in the board, 
	 * i.e., that the board is a winning board
	 */
	public boolean isWinningBoard() {
		for(int i = 0; i < ROWS; i++){
			for(int j = 0; j < COLUMNS; j++){
				if(!this.elements[i][j].isAvailable()){
					if(this.elements[i][j].getValue() == 2048){
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Moves the cells to the lowermost possible point of the game board.
	 * Movement collapses cells with the same value.
	 * It adds one more random cell with value 2 or 4, after the movement.
	 */
	public void moveDown() {
		boolean cambio = false;
		for (int col=0; col<this.numberOfColumns(); col++) {
			int floor = numberOfRows();
			for (int row=this.numberOfRows()-1; row>=0; row--) {
				if (!elements[row][col].isAvailable()) {
					int val = elements[row][col].getValue();
					int i = row+1;
					while (i<floor && elements[i][col].isAvailable()) {
						i++;
					}
					if (i==floor) {
						if (row!=floor-1) {
							elements[floor-1][col].setValue(val);
							elements[row][col].unset();
							cambio = true;
						}
					}
					else {
						int val2 = elements[i][col].getValue();
						if (val==val2) {
							elements[i][col].setValue(val+val2);
							elements[row][col].unset();
							floor--;
							cambio = true;
						}
						else {
							elements[row][col].unset();
							elements[i-1][col].setValue(val);
							if (row != i-1) cambio = true;
						}
					}
				}
			}
		}
		if (!this.isWinningBoard() && !this.isFull() && cambio) {
			Random generador = new java.util.Random();  
			int r;
			int c;
			do {
			    r =  Math.abs((generador.nextInt()%ROWS));
				c =  Math.abs((generador.nextInt()%COLUMNS));
			} while (!elements[r][c].isAvailable());
			elements[r][c].setValue((Math.abs(generador.nextInt())%2+1)*2);
			this.lastRandomCellCoordinates = new Pair(r,c);
		}
	}

	/**
	 * Moves the cells to the uppermost possible point of the game board.
	 * Movement collapses cells with the same value.
	 * It adds one more random cell with value 2 or 4, after the movement.
	 */
	public void moveUp() {
		//TODO Implement this method
	}

	/**
	 * Moves the cells to the leftmost possible point of the game board.
	 * Movement collapses cells with the same value.
	 * It adds one more random cell with value 2 or 4, after the movement.
	 */
	public void moveLeft() {
		//TODO Implement this method
	}

	/**
	 * Moves the cells to the righttmost possible point of the game board.
	 * Movement collapses cells with the same value.
	 * It adds one more random cell with value 2 or 4, after the movement.
	 */
	public void moveRight() {
		//TODO Implement this method
	}

	/**
	 * Set cell in [row,col] position with a given value
	 * @param row is the row of the cell to set
	 * @param col is the column of the cell to set
	 * @param value is the value to set the cell with
	 */
	public void setCell(int row, int col, int value) {
		if (row<0 || row>=numberOfRows()) throw new IllegalArgumentException("invalid row");
        if (col<0 || col>=numberOfColumns ()) throw new IllegalArgumentException ("invalid col");
        if (!Cell2048.isValidValue(value)) throw new IllegalArgumentException("invalid value: " + value);
        this.elements[row][col].setValue(value);
	}

	/**
	 * Sets a randomly picked free cell with a randomly chosen value in {2,4} 
	 */
	public void setRandomFreeCell() {
		//TODO implement this method
	}

	/** 
	 * Checks that a board is valid, i.e., it has all its cells non-null,
	 * with valid values in each of them (or unset).
	 * @return true iff board satisfies its internal representation
	 */
	public boolean repOK() {
		//TODO Implement this method
		return false;
	}

	public boolean onlyTwosOrFours() {
		boolean ok=true;
		for(int row=0; row<ROWS && ok;row++){
			for(int col=0; col<COLUMNS && ok;col++){
				if(!this.elements[row][col].isAvailable()) {
					if (!(this.elements[row][col].getValue()==2 || this.elements[row][col].getValue()==4))
						ok = false;
				}
							
			}
		}
		return ok;
	}

	public boolean isAvailable(int row, int col) {
		//TODO check parameters
		return this.elements[row][col].isAvailable();
	}

	public void clearCell(int row, int col) {
		//TODO check parameters
		this.elements[row][col].unset();
	}

	public int getValue(int row, int col) {
		//TODO check parameters
		return this.elements[row][col].getValue();
	}
}
