package model;

/**
 * Class that represents a cell of a 2048 board.
 *
 */
public class Cell2048 {
	
	/**
	 * Represents value stored in the cell.
	 * When the value is zero, it means that the
	 * cell is not set.
	 * Value should otherwise be a power of two (greater than one)
	 */
	private int value;

	/** 
	 * Create a new cell with default value.
	 * Cell should be unset
	 */
	public Cell2048(){
                this.value=0; 
	}

	/**
	 * creates a cell initialized with a user-provided value
	 * @param value is the value to set the cell with.
	 */
	public Cell2048(int value) {
		
                if (value>1 && isPowerOfTwo(value))
                  this.value=value;              
                else
                  throw new IllegalArgumentException("Valor invalido");
        }
	/**
	 * Provides a string representation of a cell (shows its value as a string)
	 */
	public String toString() {
            String cadena;
            if(this.value==0)
                cadena=" ";
            else 
                cadena= "" + this.value;  
            return cadena;	
	}
	
	/** 
	 * Update the value of a cell with 'value'. Provided value should be
	 * a power of two greater than or equal to 2.
	 * @param value is the user-provided value to set the cell with.
	 */
	public void setValue(int value) {
		
                if (value>1 && isPowerOfTwo(value))
                  this.value=value;              
                else
                  throw new IllegalArgumentException("Valor invalido: " + value);
	}

	/** 
	 * Updates the cell removing its contents.
	 */
	public void unset() {
            
            if(this.value!=0)
                this.value=0;
            else
                throw new IllegalStateException("Valor Invalido");
	}
	
	/** 
	 * Return value stored in the cell.
	 * Cell must not be available for this method to return a value.
	 * @return value stored in the cell, if cell is set. Throws exception otherwise
	 */
	public int getValue() {
		return this.value;
	}
	
	/** 
	 * Returns true if value is either 0, or a power of two greater than 1.
	 * @param value is the value to check
	 * @return true if is argument is 0 or a power of two greater than 1.
	 */
	public static boolean isValidValue(int value) {
		
                if ((value==0) || (value >1 && isPowerOfTwo(value)))
                    return true;
                else 
                    return false;
	}

	/** 
	 * Returns True if value is power of 2
	 * @param value is the value to check.
	 * @return true if value is a power of two.
	 */
	public static boolean isPowerOfTwo(int value) {
          if (value<0) throw new IllegalArgumentException();
          int i = 0;
          boolean res = false;
          while (((int) Math.pow(2,i))<=value && !res) {
        	  res = ((int) Math.pow(2,i))==value;
        	  i++;
          }
          return res;
	}
	
	/** 
	 * Returns true if a cell is available, that is, if it is unset.
	 * @return true iff cell is unset.
	 */
	public boolean isAvailable() {
		if(this.value==0)
                    return true;
                else
                    return false;
	}
	
	/** 
	 * Checks that a cell is valid, i.e., it has either zero or 
	 * a value that is a power of two greater than 1
	 * @return true iff cell satisfies its internal representation
	 */
	public boolean repOK() {
		if ((this.value==0) || (this.value >1 && isPowerOfTwo(this.value))) 
                    return true;
                else
                    return false;						
	}
}
