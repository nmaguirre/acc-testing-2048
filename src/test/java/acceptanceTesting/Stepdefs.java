package acceptanceTesting;

import main.Application;
import main.Pair;
import model.Board2048;
import controller.Controller2048;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.*;

public class Stepdefs {
	
	Application app;
	Pair xy;
	
	WebDriver driver;
	String geckoPath = "/Users/aguirre/Software/geckodriver/geckodriver";
	
	@Dado("^que la aplicación ha sido iniciada$")
	public void que_la_aplicación_ha_sido_iniciada() throws Exception {
		app = new Application();
	}

	@Cuando("^ejecuto el comando de inicio del juego$")
	public void ejecuto_el_comando_de_inicio_del_juego() throws Exception {
	    app.juegoNuevo();
	}

	@Entonces("^debería ver el tablero inicial, con sólo dos celdas aleatorias cargadas, con valores aleatorios, 2 o 4\\.$")
	public void deberia_haber_dos_valores() throws Exception {
	    assertEquals(2, app.getBoard().numberOfFilledCells());
	    assertTrue(app.getBoard().onlyTwosOrFours());
	}

	@Dado("^el juego ha sido iniciado$")
	public void el_juego_ha_sido_iniciado() throws Exception {
	    app.juegoNuevo();
	}

	@Dado("^el tablero está en el estado$")
	public void el_tablero_está_en_el_estado(DataTable arg1) throws Exception {
		List<List<Integer>> filas = arg1.asLists(Integer.class);
		Board2048 tablero = app.getBoard();
		for (int row = 0; row<tablero.numberOfRows(); row++) {
			List<Integer> filaCorriente = filas.get(row);
			for (int col=0; col<tablero.numberOfColumns(); col++) {
				if (!tablero.isAvailable(row, col)) {
					tablero.clearCell(row, col);
				}
				Integer valor = filaCorriente.get(col);
				if (valor!=null) {
					tablero.setCell(row, col, valor);
				}
			}
		}
	}

	@Cuando("^ejecuto el comando down$")
	public void ejecuto_el_comando_down() throws Exception {
		app.moveDown();

	}

	@Cuando("^las coordenadas aleatorias de la nueva celda a cargar son \\(x,y\\)$")
	public void las_coordenadas_aleatorias_de_la_nueva_celda_a_cargar_son_x_y() throws Exception {
		xy = app.getLastRandomCoordinates();
	}

	@Entonces("^debería obtener el tablero$")
	public void debería_obtener_el_tablero(DataTable arg1) throws Exception {
		List<List<Integer>> filas = arg1.asLists(Integer.class);
		Board2048 tablero = app.getBoard();
		for (int row = 0; row<tablero.numberOfRows(); row++) {
			List<Integer> filaCorriente = filas.get(row);
			for (int col=0; col<tablero.numberOfColumns(); col++) {
				Integer valorTabla = filaCorriente.get(col);
				if (valorTabla!=null) {
					assertFalse(tablero.isAvailable(row, col));
					int valorTablero = tablero.getValue(row, col);
					assertTrue(valorTabla == valorTablero);
				}
				else {
					if (row==xy.getX() && col==xy.getY()) {
						assertFalse(tablero.isAvailable(row, col));
					}
					else {
						assertTrue(tablero.isAvailable(row, col));
					}
				}
			}
		}	    
	}

	@Entonces("^debería obtener exactamente el tablero$")
	public void debería_obtener_exactamente_el_tablero(DataTable arg1) throws Exception {
		List<List<Integer>> filas = arg1.asLists(Integer.class);
		Board2048 tablero = app.getBoard();
		for (int row = 0; row<tablero.numberOfRows(); row++) {
			List<Integer> filaCorriente = filas.get(row);
			for (int col=0; col<tablero.numberOfColumns(); col++) {
				Integer valorTabla = filaCorriente.get(col);
				if (valorTabla!=null) {
					assertFalse(tablero.isAvailable(row, col));
					int valorTablero = tablero.getValue(row, col);
					assertTrue(valorTabla == valorTablero);
				}
				else {
					assertTrue(tablero.isAvailable(row, col));
				}
			}
		}	    
	}
	
	@Entonces("^\\(x,y\\) deben ser diferentes a \\((\\d+),(\\d+)\\)$")
	public void x_y_deben_ser_diferentes_a(int arg1, int arg2) throws Exception {
	    assertTrue(xy.getX()!=arg1 || xy.getY()!=arg2);
	}

	@Entonces("^el tablero en la posición \\(x,y\\) debería estar cargado con (\\d+) o (\\d+)\\.$")
	public void el_tablero_en_la_posición_x_y_debería_estar_cargado_con_o(int arg1, int arg2) throws Exception {
	    int x = xy.getX();
	    int y = xy.getY();
	    int valor = app.getBoard().getValue(x, y);
	    assertTrue(valor==arg1 || valor==arg2);
	}
	
	@Entonces("^debería informar al usuario que ganó$")
	public void debería_informar_al_usuario_que_ganó() throws Exception {
	    assertTrue(app.getBoard().isFinished() && app.getBoard().isWinningBoard());
	}
	
	
	@Dado("^que estoy en la página web del 2048$")
	public void que_estoy_en_la_página_web_del() throws Throwable {
		System.setProperty("webdriver.gecko.driver", geckoPath);
	    driver = new FirefoxDriver();
	    driver.get("http://2048game.com/");
	}

	@Cuando("^hago click en el boton new game$")
	public void hago_click_en_el_boton_new_game() throws Throwable {
		driver.findElement(By.className("restart-button")).click();
	}

	@Entonces("^debería ver el tablero con solo dos casilleros ocupados$")
	public void debería_ver_el_tablero_con_solo_dos_casilleros_ocupados() throws Throwable {
		List<WebElement> tiles = driver.findElements(By.className("tile-inner"));
		assertEquals(2, tiles.size());
	}

	@Entonces("^los casilleros deben tener solo (\\d+) o (\\d+)$")
	public void los_casilleros_deben_tener_solo_o(int arg1, int arg2) throws Throwable {
		List<WebElement> tiles = driver.findElements(By.className("tile-inner"));
		for (WebElement tile: tiles) {
			int value = Integer.parseInt(tile.getText());
			assertTrue(value==arg1 || value==arg2);
		}
	}
	
	@Entonces("^el browser se cierra\\.$")
	public void el_browser_se_cierra() throws Exception {
	    driver.close();
	}
	
}