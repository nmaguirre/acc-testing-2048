# language: es
Característica: El juego reacciona a los movimientos básicos, y a la lógica del juego, correctamente. 
				Esta característica corresponde a la reacción correcta al comando down (mover hacia abajo).
               
	Antecedentes:
		Dado que la aplicación ha sido iniciada
		Y el juego ha sido iniciado   
               
				@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero colapsando celdas que se chocan y tienen
        			el mismo valor    		
            Dado el tablero está en el estado
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |2 |  |
                        |  |  |2 |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |4 |  |
            Y (x,y) deben ser diferentes a (3,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.

				@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero sin colapsar celdas que se chocan y tienen
        			distinto valor
            Dado el tablero está en el estado
                        |  |  |  |  |
                        |  |  |2 |  |
                        |  |  |4 |  |
                        |  |  |  |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |2 |  |
                        |  |  |4 |  |
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (2,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.
          
          
        @ApiTest
        Escenario: Mover hacia abajo no cambia el estado si estan todos abajo
            Dado el tablero está en el estado
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |2 |4 |  |
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |2 |4 |  |
                        
        @ApiTest
        Escenario: Mover hacia abajo no cambia el estado si estan todos abajo, aunque haya adyacentes
            Dado el tablero está en el estado
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |2 |2 |  |
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |2 |2 |  |                

        @ApiTest
        Escenario: Mover hacia abajo no cambia el estado, tablero lleno, sin adyacentes verticales
            Dado el tablero está en el estado
                        | 2| 2| 2| 2|
                        | 4| 4| 4| 4|
                        | 8| 8| 8| 8|
                        |16|16|16|16|
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        | 2| 2| 2| 2|
                        | 4| 4| 4| 4|
                        | 8| 8| 8| 8|
                        |16|16|16|16|
 
        @ApiTest
        Escenario: Mover hacia abajo en tablero lleno, con celdas verticales adyacentes, no incrementa valor máximo
        		del tablero
            Dado el tablero está en el estado
                        | 2| 2| 2| 2|
                        | 4| 4| 4| 4|
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        | 2| 2| 2| 2|
                        | 4| 4| 4| 4|
                        | 4| 4| 4| 4|
            Y (x,y) deben ser diferentes a (3,0)
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (3,3)
            Y (x,y) deben ser diferentes a (2,0)
            Y (x,y) deben ser diferentes a (2,1)
            Y (x,y) deben ser diferentes a (2,2)
            Y (x,y) deben ser diferentes a (2,3)
            Y (x,y) deben ser diferentes a (1,0)
            Y (x,y) deben ser diferentes a (1,1)
            Y (x,y) deben ser diferentes a (1,2)
            Y (x,y) deben ser diferentes a (1,3)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.            
            
        @ApiTest
        Escenario: Mover hacia abajo en tablero lleno, con celdas verticales adyacentes, incrementa valor máximo
        		del tablero
            Dado el tablero está en el estado
                        | 2| 2| 2| 2|
                        | 4| 4| 4| 4|
                        | 8| 8| 8| 8|
                        | 8| 8| 8| 8|
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        | 2| 2| 2| 2|
                        | 4| 4| 4| 4|
                        |16|16|16|16|
            Y (x,y) deben ser diferentes a (3,0)
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (3,3)
            Y (x,y) deben ser diferentes a (2,0)
            Y (x,y) deben ser diferentes a (2,1)
            Y (x,y) deben ser diferentes a (2,2)
            Y (x,y) deben ser diferentes a (2,3)
            Y (x,y) deben ser diferentes a (1,0)
            Y (x,y) deben ser diferentes a (1,1)
            Y (x,y) deben ser diferentes a (1,2)
            Y (x,y) deben ser diferentes a (1,3)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.                        
            
        @ApiTest
        Escenario: Mover hacia abajo en tablero lleno, con celdas verticales adyacentes, incrementa valor máximo
        									del tablero hasta llegar a 2048.
            Dado el tablero está en el estado
                        |   2|   2|   2|   2|
                        |   4|   4|   4|   4|
                        |1024|1024|1024|1024|
                        |1024|1024|1024|1024|
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener exactamente el tablero
                        |    |    |    |    |
                        |   2|   2|   2|   2|
                        |   4|   4|   4|   4|
                        |2048|2048|2048|2048|
						Y debería informar al usuario que ganó 
            
        @ApiTest
        Escenario: Mover hacia abajo en tablero no lleno, con celdas verticales adyacentes, incrementa valor máximo
        									del tablero hasta llegar a 2048.
            Dado el tablero está en el estado
                        |    |    |    |    |
                        |   4|   4|   4|   4|
                        |1024|1024|1024|1024|
                        |1024|1024|1024|1024|
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener exactamente el tablero
                        |    |    |    |    |
                        |    |    |    |    |
                        |   4|   4|   4|   4|
                        |2048|2048|2048|2048|
						Y debería informar al usuario que ganó 
            
            
            