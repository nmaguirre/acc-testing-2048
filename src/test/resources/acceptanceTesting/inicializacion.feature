# language: es
Característica: Cuando se inicia el juego, se muestra un tablero con sólo dos casilleros ocupados, con 2 o 4.

				@ApiTest                
        Escenario: Comienzo de juego por defecto
        		Dado que la aplicación ha sido iniciada
            Cuando ejecuto el comando de inicio del juego
            Entonces debería ver el tablero inicial, con sólo dos celdas aleatorias cargadas, con valores aleatorios, 2 o 4.
