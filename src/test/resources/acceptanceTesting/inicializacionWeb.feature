# language: es
Característica: Escenarios de ejecución de inicio de 2048, con accesso directo web


	@WebTest	
	Escenario: Inicio correcto del 2048 desde la interfaz web
		Dado que estoy en la página web del 2048
		Cuando hago click en el boton new game
		Entonces debería ver el tablero con solo dos casilleros ocupados
		Y los casilleros deben tener solo 2 o 4
		Y el browser se cierra.
		
		
