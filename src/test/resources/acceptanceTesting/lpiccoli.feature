# language: es
Característica: El juego reacciona a los movimientos básicos, y a la lógica del juego, correctamente. 
				Esta característica corresponde a la reacción correcta al comando down (mover hacia abajo).
				Escenarios escritos por lpiccoli.
      
	Antecedentes:
		Dado que la aplicación ha sido iniciada
		Y el juego ha sido iniciado
	               
				@ApiTest
        Escenario: Mover hacia abajo no cambia el estado del tablero cuando todas las celdas de una columna tienen valores que no colapsan
            Y el tablero está en el estado
                        |  |  |4 |  |
                        |  |  |2 |  |
                        |  |  |4 |  |
                        |  |  |2 |  |
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        |  |  |4 |  |
                        |  |  |2 |  |
                        |  |  |4 |  |
                        |  |  |2 |  |

          
				@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero colapsando celdas que se chocan y tienen
        			el mismo valor con un espacio intermedio
            Y el tablero está en el estado
                        |  |  |2 |  |
                        |  |  |  |  |
                        |  |  |2 |  |
                        |  |  |  |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |4 |  |
            Y (x,y) deben ser diferentes a (3,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.          
            
				@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero colapsando dos pares de celdas que se chocan y tienen
        			el mismo valor
            Y el tablero está en el estado
                        |  |  |2 |  |
                        |  |  |2 |  |
                        |  |  |2 |  |
                        |  |  |2 |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |4 |  |
                        |  |  |4 |  |
            Y (x,y) deben ser diferentes a (3,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.
            
				@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero colapsando dos pares de celdas que se chocan y tienen el mismo valor pero valor diferente entre cada par
            Y el tablero está en el estado
                        |  |  |4 |  |
                        |  |  |4 |  |
                        |  |  |2 |  |
                        |  |  |2 |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |8 |  |
                        |  |  |4 |  |
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (2,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.                                   