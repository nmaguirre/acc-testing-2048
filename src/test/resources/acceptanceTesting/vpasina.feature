# language: es
Característica: El juego reacciona a los movimientos básicos, y a la lógica del juego, correctamente. 
				Esta característica corresponde a la reacción correcta al comando down (mover hacia abajo).
				Escenarios escritos por Vanina Pasina.
               
				@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero colapsando celdas que se chocan y tienen
        			el mismo valor
        		Dado que la aplicación ha sido iniciada
						Y el juego ha sido iniciado
            Y el tablero está en el estado
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |4 |  |
                        |  |  |4 |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |8 |  |
            Y (x,y) deben ser diferentes a (3,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.
            
           		@ApiTest
        Escenario: Mover hacia abajo cambia el estado del tablero colapsando celdas que se chocan y tienen
        			el mismo valor
        		Dado que la aplicación ha sido iniciada
						Y el juego ha sido iniciado
            Y el tablero está en el estado
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |4 |  |  |
                        |  |  |4 |  |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |4 |4 |  |
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,2)
            Y el tablero en la posición (x,y) debería estar cargado con 2 o 4.   